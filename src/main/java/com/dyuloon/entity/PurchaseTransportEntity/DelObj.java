package com.dyuloon.entity.PurchaseTransportEntity;

import lombok.Data;

@Data
public class DelObj {
    private Integer transportId;
    private String transportOrdernumber;
}
